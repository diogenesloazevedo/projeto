package com.banksystem.projeto.vo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

 
public class ManipuladorArquivo {		// Classe com m�todos de leitura e escrita
 
    public static String  leitor(File file, String json) throws IOException {		// Leitura do arquivo
        BufferedReader buffRead = new BufferedReader(new FileReader(file));
        String linha = "";
        while (true) {
            if (linha != null) {
                if (linha.equals(json)){
                	String aux = buffRead.readLine();
                	buffRead.close();
                	return aux;
                	
                }             
 
            } else
                break;
            linha = buffRead.readLine();
        }
        buffRead.close();
		return null;
    }
    
 
    public static void escritor(File file, String json) throws IOException {	 	// Escrita no arquivo
        BufferedWriter buffWrite = new BufferedWriter(new FileWriter(file, true));
        buffWrite.write(json);
        buffWrite.newLine();
        buffWrite.close();
    }
 
}
