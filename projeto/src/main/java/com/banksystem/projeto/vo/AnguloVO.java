package com.banksystem.projeto.vo;

public class AnguloVO {		// Classe Angulo, para retorno do Json
	private int angulo;

	public int getAngulo() {
		return angulo;
	}

	public void setAngulo(int angulo) {
		this.angulo = angulo;
	}
}
