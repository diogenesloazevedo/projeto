package com.banksystem.projeto.service;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.banksystem.projeto.vo.AnguloVO;
import com.banksystem.projeto.vo.ManipuladorArquivo;
import com.banksystem.projeto.vo.RelogioVO;
import com.google.gson.Gson;

@Path("/clock")
public class RelogioService {				// Classe onde o servi�o e feiro
	File arq = new File("consultas.txt");	// Arquivo para guardar as consultas
	

	@GET()
	@Path("/{hora}/{minutos}")		// Formato da URL com duas entradas
	@Produces({MediaType.APPLICATION_JSON})
	public String retornaAngulo(@PathParam("hora") int hora,@PathParam("minutos") int minutos) throws IOException{  // Servi�o para duas entradas
		
		RelogioVO vo = new RelogioVO();		// Criacao do objeto RelogioVO
		vo.setHora(hora);
		vo.setMinutos(minutos);
		Gson gson = new Gson();
		String mGson = gson.toJson(vo);
		
		if (arq.exists()){				// Verifica se o arquivo existe	
			String consulta = ManipuladorArquivo.leitor(arq, mGson);	// Le o arquivo em busca da consulta realizada
			
			if (consulta != null){		// Se achar a consulta, retorna sem precisar calcular o angulo
				return consulta;
			}
			
			else{		// Se n�o achar, calcula o angulo, escreve no arquivo e retorna o objeto Angulo como Json
				int mArcoH = vo.getHora() * 30;
				int mArcoM = vo.getMinutos() * 6;
				int angulo = Math.abs(mArcoM-mArcoH);
				
				if(angulo <= 180){
					vo.setAngulo(angulo);
				}else{
					angulo = 360 - angulo;
					vo.setAngulo(angulo);
				}
				AnguloVO ang = new AnguloVO();
				ang.setAngulo(vo.getAngulo());
				Gson gsonC = new Gson();
				String jsonAng = gsonC.toJson(ang);
				ManipuladorArquivo.escritor(arq, mGson);
				ManipuladorArquivo.escritor(arq, jsonAng);
				return jsonAng;
			}
		}
		
		else{		// Arquivo nao existe => Nenhuma consulta realizada, calcula o angulo e escreve no arquivo
			int mArcoH = vo.getHora() * 30;
			int mArcoM = vo.getMinutos() * 6;
			int angulo = Math.abs(mArcoM-mArcoH);
			
			if(angulo <= 180){
				vo.setAngulo(angulo);
			}else{
				angulo = 360 - angulo;
				vo.setAngulo(angulo);
			}
			AnguloVO ang = new AnguloVO();
			ang.setAngulo(vo.getAngulo());
			Gson gsonC = new Gson();
			String jsonAng = gsonC.toJson(ang);
			ManipuladorArquivo.escritor(arq, mGson);
			ManipuladorArquivo.escritor(arq, jsonAng);
			return jsonAng;
		}
		
	}
	
	
	
	@GET()
	@Path("/{hora}")		// Formato da URL com uma entrada
	@Produces({MediaType.APPLICATION_JSON})
	public String retornaAngulo2(@PathParam("hora") int hora) throws IOException{  // Servi�o para duas entradas
		
		RelogioVO vo = new RelogioVO();		// Criacao do objeto RelogioVO
		vo.setHora(hora);
		vo.setMinutos(0);
		Gson gson = new Gson();
		String mGson = gson.toJson(vo);
		
		if (arq.exists()){				// Verifica se o arquivo existe	
			String consulta = ManipuladorArquivo.leitor(arq, mGson);	// Le o arquivo em busca da consulta realizada
			
			if (consulta != null){		// Se achar a consulta, retorna sem precisar calcular o angulo
				return consulta;
			}
			
			else{		// Se n�o achar, calcula o angulo, escreve no arquivo e retorna o objeto Angulo como Json
				int mArcoH = vo.getHora() * 30;
				int mArcoM = 0;
				int angulo = Math.abs(mArcoM-mArcoH);
				
				if(angulo <= 180){
					vo.setAngulo(angulo);
				}else{
					angulo = 360 - angulo;
					vo.setAngulo(angulo);
				}
				AnguloVO ang = new AnguloVO();
				ang.setAngulo(vo.getAngulo());
				Gson gsonC = new Gson();
				String jsonAng = gsonC.toJson(ang);
				ManipuladorArquivo.escritor(arq, mGson);
				ManipuladorArquivo.escritor(arq, jsonAng);
				return jsonAng;
			}
		}
		
		else{		// Arquivo nao existe => Nenhuma consulta realizada, calcula o angulo e escreve no arquivo
			int mArcoH = vo.getHora() * 30;
			int mArcoM = 0;
			int angulo = Math.abs(mArcoM-mArcoH);
			
			if(angulo <= 180){
				vo.setAngulo(angulo);
			}else{
				angulo = 360 - angulo;
				vo.setAngulo(angulo);
			}
			AnguloVO ang = new AnguloVO();
			ang.setAngulo(vo.getAngulo());
			Gson gsonC = new Gson();
			String jsonAng = gsonC.toJson(ang);
			ManipuladorArquivo.escritor(arq, mGson);
			ManipuladorArquivo.escritor(arq, jsonAng);
			return jsonAng;
		}
		
	}

}
